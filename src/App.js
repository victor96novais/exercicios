// Importa o React e o componente da biblioteca react
import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import Simples from './componentes/Simples'
import ParImpar from './componentes/ParImpar'
import { Inverter, MegaSena } from './componentes/Multi'

// Export exporta a classe para o index.js recebe-la
export default class App extends Component {
	// Função render para renderizar a tela
	render(){
		return (
			<View style={styles.container}>
				<Simples texto='Flexível!!!'/>
				<ParImpar numero={30}/>
				<Inverter texto='React Nativo!'/>
				<MegaSena numeros={6} />
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	}
})